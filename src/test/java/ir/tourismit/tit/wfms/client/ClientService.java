package ir.tourismit.tit.wfms.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import ir.tourismit.tit.wfms.*;

import java.util.Date;


public class ClientService {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9099)
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub stub
                = ServiceGrpc.newBlockingStub(channel);

        MessageResponse response = stub.send(MessageInfo.newBuilder()
                .setFromId("U1")
                .setToId("U15")
                .setBody("test01")
                .setDate(new Date().toString())
                .build());
        System.out.println(response);

        Messages messages = stub.getMessages(Chat.newBuilder()
                .setFromId("U1")
                .setToId("U2")
                .build());
        System.out.println(messages);

        Users users = stub.openChats(UserInfo.newBuilder().setUserId("U1").build());
        System.out.println(users);

        channel.shutdown();
    }
}
