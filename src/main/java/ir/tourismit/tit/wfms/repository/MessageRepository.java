package ir.tourismit.tit.wfms.repository;


import ir.tourismit.tit.wfms.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    Message save(Message msg);

    List<Message> getAllByFromIdAndToId(String fromId, String toId);

    @Query("select toId from Message where fromId = :userId")
    List<String> getToIdByFromId(@Param("userId") String toId);

    @Query("select fromId from Message where toId = :userId")
    List<String> getFromIdByToId(@Param("userId") String toId);
}
