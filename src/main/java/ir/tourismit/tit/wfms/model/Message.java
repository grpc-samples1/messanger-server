package ir.tourismit.tit.wfms.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "message")
@Embeddable
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String fromId;
    private String toId;
    private String date;
    private String body;

    public Message() {

    }

    public Message(String fromId, String toId, String date, String body) {
        this.fromId = fromId;
        this.toId = toId;
        this.date = date;
        this.body = body;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Message))
            return false;
        if (!((Message) obj).date.equals(date))
            return false;
        if (!((Message) obj).fromId.equals(fromId))
            return false;
        if (!((Message) obj).toId.equals(toId))
            return false;
        return ((Message) obj).body.equals(body);
    }
}
