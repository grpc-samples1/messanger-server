package ir.tourismit.tit.wfms.service.grpc;

import io.grpc.stub.StreamObserver;
import ir.tourismit.tit.wfms.*;
import ir.tourismit.tit.wfms.model.Message;
import ir.tourismit.tit.wfms.repository.MessageRepository;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@GrpcService
public class ServiceImpl extends ServiceGrpc.ServiceImplBase {

    @Autowired
    private MessageRepository messageRepository;

    @Override
    public void getMessages(Chat request, StreamObserver<Messages> responseObserver) {
        String fromId = request.getFromId();
        String toId = request.getToId();
        List<Message> messages = messageRepository.getAllByFromIdAndToId(fromId, toId);
        List<MessageInfo> messageInfos = messages.stream().map(m -> MessageInfo.newBuilder()
                .setFromId(m.getFromId())
                .setToId(m.getToId())
                .setBody(m.getBody())
                .setDate(m.getDate())
                .build()).collect(Collectors.toList());
        Messages response = Messages.newBuilder().addAllMessage(messageInfos).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void send(MessageInfo request, StreamObserver<MessageResponse> responseObserver) {
        System.out.println(request);
        messageRepository.save(new Message(request.getFromId(), request.getToId(), request.getDate(), request.getBody()));
        MessageResponse response = MessageResponse.newBuilder()
                .setStatus(200)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void openChats(UserInfo request, StreamObserver<Users> responseObserver) {
        String userId = request.getUserId();
        Set<String> userIds = new HashSet<>(messageRepository.getToIdByFromId(userId));
        userIds.addAll(messageRepository.getFromIdByToId(userId));
        Users response = Users.newBuilder().addAllUser(userIds).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
